Vuka offers dynamic spaces that are used for public and private events, shared workspace, and offices. We are focused on community and collaboration to foster connection and drive impact. Our customers like to work in a place where they can freely express themselves while focusing on their goals.

Address: 5540 N Lamar Blvd, Austin, TX 78751, USA

Phone: 512-865-6178

Website: https://vuka.co
